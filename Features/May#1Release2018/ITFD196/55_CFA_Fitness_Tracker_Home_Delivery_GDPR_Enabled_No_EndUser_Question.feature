Feature: 55_CFA_Fitness_Tracker_Home_Delivery_GDPR_Enabled_No_EndUser_Question

  @Web
  Scenario Outline: 55_CFA_Fitness_Tracker_Home_Delivery_GDPR_Enabled_No_EndUser_Question
    Given I am an CFA user and Lands on shop page
    And Navigate to Fitness Trackers
    And select any available <Device> Fitness tracker
    And add FitnessTracker to basket within limit in details page and navigate to basket
    And I Land on the Non Phone related basket page and choose home delivery option
    And click on "go to checkout" button
    And input <Firstname> and <Surname> and other valid details in Delivery page to verify GDPR
    #customer is either Me or Someone else
    And Is this order for You or Someone else <consumer> when GDPR is <status>
    And land on the Non Credit check payment page and input <Username> and other details and click 'Continue on next step'
    And Continue to Review page and review the order
    Then order confirmation is displayed
    Then Choose <consumer> <status> Business preferences <B1> <B2> <B3> <B4> And Channel Preferences <Text> <Email> <Phone> <Post> <MBBStatus> <DeviceType>


    Examples:
      | Device  | Firstname | Surname | Username     | consumer | B1  | B2  | B3  | B4  | Text | Email | Phone | Post | status  | MBBStatus | DeviceType    |
      | iPhone 6 | TEST      | ACCEPTA | TEST ACCEPTA | Me       | Not | Not | Not | Not | Not  | Not   | Not   | Not  | Enabled | No        | Non Connected |